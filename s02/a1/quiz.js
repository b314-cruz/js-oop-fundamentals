/* ACTIVITY S02 - QUIZ */
/*
	1. What is the term given to unorganized code that's very hard to work with?
	2. How are object literals written in JS?
	3. What do you call the concept of organizing information and functionality to belong to an object?
	4. If studentOne has a method named enroll(), how would you invoke it?
	5. True or False: Objects can have objects as properties.
	6. What is the syntax in creating key-value pairs?
	7. True or False: A method can have no parameters and still work.
	8. True or False: Arrays can have objects as elements.
	9. True or False: Arrays are objects.
	10. True or False: Objects can have arrays as properties.

*/

// ANSWERS
/*
	1. Spaghetti code
	2. Using {} or a curly bracket pair.
	3. Encapsulation
	4. By using dot notation. Ex: `studentOne.enroll()``
	5. True.
	6. Key-value pairs in JavaScript objects are written as `key : value`
	7. True.
	8. True.
	9. True.
	10. True
*/