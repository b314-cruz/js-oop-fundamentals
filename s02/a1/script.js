/* ACTIVITY - FUNCTION CODING */
//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

let studentOne = {

	// properties
	name : 'John',
	email : 'john@mail.com',
	grades : [89, 84, 78, 88],

	// methods
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade averages are ${this.grades}`);
	},
	computeAve() {
		let sum = 0;

		this.grades.forEach(function(num) {
			sum += num;
		});

		avg = sum / this.grades.length;

		return avg;
	},
	willPass() {
		if (this.computeAve() >= 85) {
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else if (this.willPass() === true && (this.computeAve() >= 85 && this.computeAve() < 90)) {
			return false;
		} else if (this.computeAve() < 85) {
			return undefined;
		}
	}

};
console.log(studentOne);
console.log(studentOne.computeAve());
console.log(studentOne.willPass());
console.log(studentOne.willPassWithHonors());

let studentTwo = {

	// properties
	name : 'Joe',
	email : 'joe@mail.com',
	grades : [78, 82, 79, 85],

	// methods
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade averages are ${this.grades}`);
	},
	computeAve() {
		let sum = 0;

		this.grades.forEach(function(num) {
			sum += num;
		});

		avg = sum / this.grades.length;

		return avg;
	},
	willPass() {
		if (this.computeAve() >= 85) {
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else if (this.willPass() === true && (this.computeAve() >= 85 && this.computeAve() < 90)) {
			return false;
		} else if (this.computeAve() < 85) {
			return undefined;
		}
	}

};
console.log(studentTwo);
console.log(studentTwo.computeAve());
console.log(studentTwo.willPass());
console.log(studentTwo.willPassWithHonors());

let studentThree = {

	// properties
	name : 'Jane',
	email : 'jane@mail.com',
	grades : [87, 89, 91, 93],

	// methods
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade averages are ${this.grades}`);
	},
	computeAve() {
		let sum = 0;

		this.grades.forEach(function(num) {
			sum += num;
		});

		avg = sum / this.grades.length;

		return avg;
	},
	willPass() {
		if (this.computeAve() >= 85) {
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else if (this.willPass() === true && (this.computeAve() >= 85 && this.computeAve() < 90)) {
			return false;
		} else if (this.computeAve() < 85) {
			return undefined;
		}
	}

};
console.log(studentThree);
console.log(studentThree.computeAve());
console.log(studentThree.willPass());
console.log(studentThree.willPassWithHonors());

let studentFour = {

	// properties
	name : 'Jessie',
	email : 'jessie@mail.com',
	grades : [91, 89, 92, 93],

	// methods
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade averages are ${this.grades}`);
	},
	computeAve() {
		let sum = 0;

		this.grades.forEach(function(num) {
			sum += num;
		});

		avg = sum / this.grades.length;

		return avg;
	},
	willPass() {
		if (this.computeAve() >= 85) {
			return true;
		} else {
			return false;
		}
	},
	willPassWithHonors() {
		if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else if (this.willPass() === true && (this.computeAve() >= 85 && this.computeAve() < 90)) {
			return false;
		} else if (this.computeAve() < 85) {
			return undefined;
		}
	}

};
console.log(studentFour);
console.log(studentFour.computeAve());
console.log(studentFour.willPass());
console.log(studentFour.willPassWithHonors());

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {

	// property (student objects array)
	students : [
		studentOne,
		studentTwo,
		studentThree,
		studentFour
	],

	// methods
	countHonorStudents() {
		let count = 0;
		this.students.forEach(student => {
			if (student.willPassWithHonors()) {
				count++;
			}
		})
		return count;
	},
	honorsPercentage() {
		let percentage = (this.countHonorStudents() / this.students.length) * 100;
		return percentage;
	},
	retrieveHonorStudentInfo() {
		let honorStudents = [];
		this.students.forEach(student => {
			if (student.willPassWithHonors()) {
				honorStudents.push({
					email: student.email,
					aveGrade: student.computeAve()
				});
			}
		})
		return honorStudents;
	},
	sortHonorStudentsByGradeDesc() {
		return this.retrieveHonorStudentInfo().sort(function(a, b){return b-a});
	}
};
console.log(classOf1A);
console.log(classOf1A.countHonorStudents());
console.log(classOf1A.honorsPercentage());
console.log(classOf1A.retrieveHonorStudentInfo());
console.log(classOf1A.sortHonorStudentsByGradeDesc());