/* CUSTOM OBJECTS - JAVASCRIPT OOP FUNDAMENTALS */

// WHAT IS OOP?
/*
	A way of programming applications where reusable blueprints called classes are used to create objects.

	Benefits:
	- Minimizes repetition
	- Improves scalability
	- Improves maintainability
	- Improves security

	Object creation is at the heart of OOP. Let's look at how we can define our own custom objects next.

*/

// OBJECT LITERALS, ENCAPSULATION, AND "THIS" KEYWORD
/*
	We have seen that JS comes with predefined objects. We can also create objects of our own through the use of object literals.
*/
/*
	- use an object literal: {} to create an object representing a user
	- encapsulation: whenever we add properties or methods to an object, we are already performing this concept
	- the organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them
	- (the scope of encapsulation is denoted by object literals)
*/
let studentOne = {

	// properties
	name : 'John',
	email : 'john@mail.com',
	grades : [89, 84, 78, 88],

	// methods
	// add the functionalities available to a student as object methods, and the keyword "this" refers to the object encapsulating the method where "this" is called
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		/*grades.forEach(grade => {
		    console.log(grade);
		})*/
		console.log(`${this.name}'s' quarterly grade averages are ${this.grades}`);
	},

	/* MINI ACTIVTIY */
	// 1) Create a function that will get the quarterly average of studentOne's grades
	computeAve() {
		let sum = 0;

		this.grades.forEach(function(num) {
			sum += num;
		});

		avg = sum / this.grades.length;

		return avg;
	},
	// 2) Create a function that will return true if the average grade is >=85, otherwise false
	willPass() {
		if (this.computeAve() >= 85) {
			return true;
		} else {
			return false;
		}
	},
	// 3) Create a function called willPassWithHonors() that returns true if the student has passed and their average is >=90
	willPassWithHonors() {
		if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else {
			return false;
		}
	}

};
// log the content of studentOne's encapsulated information in the console
// console.log(studentOne);
// console.log(`Student #1's name is ${studentOne.name}`);
// console.log(`Student #1's email is ${studentOne.email}`);
// console.log(`Student #1's quarterly grade averages are ${studentOne.grades}`);
console.log(studentOne);
console.log(studentOne.computeAve());
console.log(studentOne.willPass());
console.log(studentOne.willPassWithHonors());
