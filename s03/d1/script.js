/* CLASSES - JAVASCRIPT OOP FUNDAMENTALS */

// WHAT IS A CLASS?
/*
	A class is a blueprint that describes an object in a non-specific way.

	EXAMPLE 1
	Object: Dog
	Properties: Breed, Size, Age, Color
	Methods: Eat(), Sleep(), Sit(), Run()

	EXAMPLE 2
	Class: Person
	Instances: Person 1 & Person 2 (with differing properties such as name, email, phone number)
*/

// WHY USE CLASSES?
/*
	We saw how repetitive it is to define objects each time we needed a new student. Using a class will make our code reusable.
*/

// CLASS CONSTRUCTORS & OBJECT INSTANTIATION
/*
	In JavaScript, classes can be created by using the keyword "class" and the {} or curly braces.
	Naming convention of classes: Using PascalCase or begins with uppercase letters
	Syntax:
		class <Name>{
			// properties or methods here
		}
*/
// Student Class
class Student {
	/*
		To enable students instantiated from this class to have distinct names and emails
		Our constructor must be able to acept name and email arguments, whic it will then use to set the value of the object's corresponding properties
	*/
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		/*
			Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.
		*/
		this.grades = this.validateGrades(grades);
	}
	validateGrades(grades) {
	    if ( (Array.isArray(grades)) && (grades.length === 4) && (grades.every((grade) => grade >= 0 && grade <= 100)) ) {
	    	return grades;
	    } else {
	    	return undefined;
		}
	}

	/*
		Class methods: these are common to all instances
	*/
	/*
		The login() method logs a message to the console indicating that the student with the specific email has logged in.
		The logout() method logsa message to the console indicating that the student with the specific email has logged out.
		The listGrades() method logs a message to the console displaying the name of the student and their quarterly grade averages.
		The computeAve() method calculates the average of the student's grades by iterating over the this.grades array and summing up all the grades.

		The result is stored in the this.gradeAve property.

		The willPass() method determines if the student passed or failed based on their average grade. It calls the computeAve() method to calculate the average grade and then checks if the gradeAve property is greater than or equal to 85. The result is stored in the this.passed property.

		The willPassWithHonors() method determines if the student passed with honors based on their average grade. It first checks if the student passed (this.passed is true) and then checks if the gradeAve property is greater than or equal to 90. The result is stored in the this.passedWithHonors property.
	*/
	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout() {
		console.log(`${this.email} has logged out`);
		return this;
	}

	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
		return this;
	}

	computeAve() {
		let sum = 0;

		this.grades.forEach(function(num) {
			sum += num;
		});

		return sum / this.grades.length;
	}

	willPass() {
		if (this.computeAve() >= 85) {
			return true;
		} else {
			return false;
		}
	}

	willPassWithHonors() {
		/*if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else if (this.willPass() === true && (this.computeAve() >= 85 && this.computeAve() < 90)) {
			return false;
		} else if (this.computeAve() < 85) {
			return undefined;
		}*/
		if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else {
			return false;
		}
	}
};

// Instances of Student Class
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);