/* CLASSES - JAVASCRIPT OOP FUNDAMENTALS: ACTIVITY */

class Student {

	// Class constructor with properties
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;

		this.grades = this.validateGrades(grades);
	}
	/*
		Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.
	*/
	validateGrades(grades) {
	    if ( (Array.isArray(grades)) && (grades.length === 4) && (grades.every((grade) => grade >= 0 && grade <= 100)) ) {
	    	return grades;
	    } else {
	    	return undefined;
		}
	}

	// Class methods
	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout() {
		console.log(`${this.email} has logged out`);
		return this;
	}

	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
		return this;
	}

	computeAve() {
		let sum = 0;

		this.grades.forEach(function(num) {
			sum += num;
		});

		return sum / this.grades.length;
	}

	willPass() {
		if (this.computeAve() >= 85) {
			return true;
		} else {
			return false;
		}
	}

	willPassWithHonors() {
		if (this.willPass() === true && this.computeAve() >= 90) {
			return true;
		} else {
			return false;
		}
	}
};

// Instances of Student Class
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

// QUESTIONS:
/*
	1) What is the blueprint where objects are created from? 
	2) What is the naming convention applied to classes?
	3) What keyword do we use to create objects from a class?
	4) What is the technical term for creating an object from a class?
	5) What class method dictates HOW objects will be created from that class?
*/
// ANSWERS:
/*
	1) Class.
	2) Pascal Case.
	3) 'new' keyword.
	4) Instantiation.
	5) Constructor method.
*/