/*
	QUIZ:
	1) How do you create arrays in JS?
	2) How do you access the first character of an array?
	3) How do you access the last character of an array?
	4) What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
	5) What array method loops over all elements of an array, performing a user-defined function on each iteration?
	6) What array method creates a new array with elements obtained from a user-defined function?
	7) What array method checks if all its elements satisfy a given condition?
	8) What array method checks if at least one of its elements satisfies a given condition?
	9) True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
	10) True or False: array.slice() copies elements from original array and returns them as a new array.
*/

/*
	ANSWERS:
	1) By declaring using square bracket values/set with `const arrayName = []` or through the 'new Array' keyword `const cars = new Array()``
	2) By calling the 0 index of an array. Example: arrayName[0] 
	3) By calling the last index of an array using the length property and subtracting it to one. Example: arrayName[length-1]
	4) The indexOf() array method.
	5) The forEach() array method which include a key-value format.
	6) The map() method, which includes a key-value format just like forEach.
	7) Using the every() array method.
	8) Using the some() array method.
	9) False, since splice() array method changes the contents of the original array.
	10) True, since the slice() array method does not change the original array and returns it with a new array.
*/

/*
	FUNCTION CODING

	1) Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

*/
const students = ['John', 'Joe', 'Jane', 'Jessie'];
/*
	Output:
	addToEnd(students, "Ryan"); //["John", "Joe", "Jane", "Jessie", "Ryan"]
	addToEnd(students, 045); //"error - can only add strings to an array"
*/
// ANSWER:
function addToEnd(arr, name) {
	if ((isNaN(name))) {
		arr.push(name);
		return arr;
	} else {
		return "error - can only add strings to an array";
	}
}
console.log(addToEnd(students, "Ryan"));
console.log(addToEnd(students, 045));

/* 
	2) Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.


	// Output
	addToStart(students, "Tess"); //["Tess", "John", "Joe", "Jane", "Jessie", "Ryan"]
	// validation check
	addToStart(students, 033); //"error - can only add strings to an array"
*/
function addToStart(arr, name) {
	if ((isNaN(name))) {
		arr.unshift(name);
		return arr;
	} else {
		return "error - can only add strings to an array";
	}
}
console.log(addToStart(students, "Tess"));
console.log("Validation: ", addToStart(students, 033));

/*
	3) Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

	//test input
	elementChecker(students, "Jane"); //true
	//validation check
	elementChecker([], "Jane"); //"error - passed in array is empty"

*/
// ANSWER:
function elementChecker(arr, name) {
	if (arr.length > 0) {
		return students.includes(name);
	} else {
		return "error - passed in array is empty";
	}
}
console.log(elementChecker(students, "Jane"));
console.log("Validation: ", elementChecker([], "Jane"));

/*
	4) Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:


	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	if every element in the array ends in the passed in character, return true. Otherwise return false.

	Use the students array and the character "e" as arguments when testing.

	//test input
	checkAllStringsEnding(students, "e"); //false
	//validation checks
	checkAllStringsEnding([], "e"); //"error - array must NOT be empty"
	checkAllStringsEnding(["Jane", 02], "e"); //"error - all array elements must be strings"
	checkAllStringsEnding(students, 4); //"error - 2nd argument must be of data type string"
	checkAllStringsEnding(students, "el"); //"error - 2nd argument must be a single character"

*/
// ANSWER:
function checkAllStringsEnding(arr, chara) {
	if (arr.length === 0) {
		return "error - array must NOT be empty";
	} else if (!(arr.every(i => typeof i === "string"))) {
		return "error - all array elements must be strings";
	} else if (!((typeof chara) === "string")) {
		return "error - 2nd argument must be of data type string"
	} else if (chara.length !== 1) {
		return "error - 2nd argument must be a single character"
	} else if (arr.every(i => i.endsWith(chara))) {
		return true;
	} else {
		return false;
	}
}
console.log(checkAllStringsEnding(students, "e"));
console.log("Validation 1: ", checkAllStringsEnding([], "e"));
console.log("Validation 2: ", checkAllStringsEnding(["Jane", 02], "e"));
console.log("Validation 3: ", checkAllStringsEnding(students, 4));
console.log("Validation 4: ", checkAllStringsEnding(students, "el"));

/*
	5) Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

	//test input
	stringLengthSorter(students); //["Joe", "Tess", "John", "Jane", "Ryan", "Jessie"]
	//validation check
	stringLengthSorter([037, "John", 039, "Jane"]); //"error - all array elements must be strings"

*/
// ANSWER:
function stringLengthSorter(arr) {
	if ((arr.every(i => typeof i === "string"))) {
		return arr.sort((a, b) => a.length - b.length);
	} else {
		return "error - all array elements must be strings";
	}
}
console.log(stringLengthSorter(students));
console.log(stringLengthSorter([037, "John", 039, "Jane"]));

/*
	6) Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:


	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	return the number of elements in the array that start with the character argument, must be case-insensitive

	Use the students array and the character "J" as arguments when testing.

	//test input
	startsWithCounter(students, "j"); //4
*/
// ANSWER:
function startsWithCounter(arr, chara) {
	if (arr.length === 0) {
		return "error - array must NOT be empty";
	} else if (!(arr.every(i => typeof i === "string"))) {
		return "error - all array elements must be strings";
	} else if (!((typeof chara) === "string")) {
		return "error - 2nd argument must be of data type string"
	} else if (chara.length !== 1) {
		return "error - 2nd argument must be a single character"
	} else {
		return arr.filter(x => x.startsWith(chara.toUpperCase()));
	}
}
console.log(startsWithCounter(students, "j"));

/*
	7) Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:


	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

	Use the students array and the string "jo" as arguments when testing.

	//test input
	likeFinder(students, "jo"); //["Joe", "John"]

*/
// ANSWER:
function likeFinder(arr, chara) {
	if (arr.length === 0) {
		return "error - array must NOT be empty";
	} else if (!(arr.every(i => typeof i === "string"))) {
		return "error - all array elements must be strings";
	} else if (!((typeof chara) === "string")) {
		return "error - 2nd argument must be of data type string"
	} else {
		let newStr = chara.split(' ').map(w => w[0].toUpperCase() + w.substring(1).toLowerCase()).join(' ');
		return arr.filter(i => i.includes(newStr));
	}
}
console.log(likeFinder(students, "jo"));

/*
	8) Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
	// test input
	randomPicker(students); // "Ryan"
	randomPicker(students); // "John"
	randomPicker(students); // "Jessie"
*/
// ANSWER:
function randomPicker(arr) {
	let index = Math.round(Math.random()*(arr.length-1));
	return arr[index];
}
console.log(randomPicker(students));
console.log(randomPicker(students));
console.log(randomPicker(students));